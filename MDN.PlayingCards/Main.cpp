
// Playing Cards
// Matt Nelson

#include <iostream>
#include <conio.h>

using namespace std;

enum Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum Suit
{
	HEARTS,
	DIAMONDS,
	CLUBS,
	SPADES
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{
	Card card;
	card.rank = THREE;
	card.suit = DIAMONDS;

	//cout << card.rank << ", " << card.suit;

	_getch();
	return 0;
}
